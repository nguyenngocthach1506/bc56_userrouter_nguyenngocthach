import React from 'react';
import "./style.css";

export default function Loading() {
    return (
        <div className="loading">
            < div className="spinner-border text-warning" role="status" >
                <span className="visually-hidden">Loading...</span>
            </div>
        </div>
    )
}
