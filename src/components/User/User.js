import React, { Component } from 'react';
import { connect } from 'react-redux';
import { message } from 'antd';
import { getUserAction } from '../../redux/action/userAction';
import userApi from '../../service/Api/userApi';
import TYPE from '../../redux/constaint/type';


class User extends Component {
    componentDidMount() {
        this.props.getListUser();
    }

    handleGetUserToDetail(user) {
        this.props.getUserDetail(user);
    }

    handleDeleteUser = async (id) => {
        this.props.setLoading(true);
        const respone = await userApi.removeUser(id);

        if (respone !== undefined) {
            this.props.getListUser();
            message.success("Xóa thành công");
        }

        this.props.setLoading(false);
    }

    renderListUser = (list = []) => {
        return list.reverse().map((user, index) => {
            return (
                <tr key={"userItem-" + index}>
                    <td className='text-center'>{user.id}</td>
                    <td>{user.name}</td>
                    <td>{user.account}</td>
                    <td>{user.password}</td>
                    <td className='text-center'>
                        <a onClick={() => this.handleDeleteUser(user.id)} className='fs-6 text-danger mx-1' role='button'><i className="bi bi-trash-fill"></i></a>
                        <a onClick={() => this.handleGetUserToDetail(user)} className='fs-6 text-success mx-1' role='button'><i className="bi bi-pencil-square"></i></a>
                    </td>
                </tr>
            );
        })
    }


    render() {
        return (
            <div className="table-responsive">
                <table className='table table-sm align-middle table-striped table-hover'>
                    <thead>
                        <tr>
                            <th className='px-3 text-center'>Id</th>
                            <th className='text-start'>Name</th>
                            <th className='text-start'>Account</th>
                            <th className='text-start'>Password</th>
                            <th className='text-center'>Settings</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderListUser(this.props.users)}
                    </tbody>
                </table>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.userReducer.users,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getListUser: () => {
            dispatch(getUserAction());
        },
        getUserDetail: (userDetail = {}) => {
            let action = {
                type: TYPE.GET_USER_DETAIL,
                payload: userDetail,
            }
            dispatch(action);
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(User);