import React, { Component, createRef } from 'react';
import { connect } from 'react-redux';
import { message } from 'antd';

import TYPE from '../../redux/constaint/type';
import userApi from "../../service/Api/userApi";
import { getUserAction } from "../../redux/action/userAction";


class Form extends Component {
    inputRef = createRef();

    handleChangeInput = (event) => {
        /* Element input */
        const input = event.target;
        /* set newUser in userDetail */
        this.props.getUserDetail({ ...this.props.userDetail, [input.name]: input.value })
    }

    handleAddUser = async () => {
        const { userDetail } = this.props;

        if (userDetail.name == '' && userDetail.account == '' && userDetail.password == '') {
            message.error("Thông tin trống!");
            this.props.setLoading(false);
            return;
        }

        this.props.setLoading(true);    // Turn on Loading

        const reponse = await userApi.postUser(userDetail);
        /* Post new user to Api */

        if (reponse !== undefined) {
            this.props.getListUser();
            message.success("Thêm thành công");
            this.inputRef.current.focus();
            /* Reset userDetail from Redux */
            this.props.getUserDetail({ id: "", name: "", account: "", password: "" });
        }
        else message.error("Thêm KHÔNG thành công!");

        this.props.setLoading(false);   // Turn off Loading
    }

    handleUpdateUser = async () => {
        const { userDetail } = this.props;

        if (userDetail.name == '' && userDetail.account == '' && userDetail.password == '') {
            message.error("Thông tin trống!");
            this.props.setLoading(false);
            return;
        }

        this.props.setLoading(true);    //Turn on Loading

        /* Put new user to Api */
        const reponse = await userApi.putUser(userDetail.id, userDetail);

        if (reponse !== undefined) {
            this.props.getListUser();
            message.success("Cập nhật thành công");
            /* Reset userDetail from Redux */
            this.props.getUserDetail({ id: "", name: "", account: "", password: "" });
        }
        else message.error("Vui lòng chọn User để cập nhật!");

        this.props.setLoading(false);   // Turn off Loading
    }


    render() {
        let { userDetail } = this.props;
        return (
            <form className='form w-75 mx-auto'>
                <div className='row row-cols-1 row-cols-lg-2'>
                    <div className='form-group col py-2'>
                        <label className='form-label text-secondary'>Name : </label>
                        <input ref={this.inputRef} onChange={(e) => this.handleChangeInput(e)} className='form-control' type='text' name="name" value={userDetail.name} />
                    </div>
                    <div className='form-group col py-2'>
                        <label className='form-label text-secondary'>Account : </label>
                        <input onChange={(e) => this.handleChangeInput(e)} className='form-control' type='text' name="account" value={userDetail.account} />
                    </div>
                    <div className='form-group col py-2'>
                        <label className='form-label text-secondary'>Password : </label>
                        <input onChange={(e) => this.handleChangeInput(e)} className='form-control' type='text' name="password" value={userDetail.password} />
                    </div>
                </div>

                <div className='form-group py-2'>
                    <button onClick={this.handleAddUser} className='btn btn-primary fs-6' type='button'>Add</button>
                    <button onClick={this.handleUpdateUser} className='btn btn-dark mx-5 fs-6' type='button'>Update</button>
                </div>
            </form >
        )
    }
}


const mapStateToProps = (state) => {
    return {
        userDetail: state.userReducer.userDetail,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getListUser: () => {
            dispatch(getUserAction());
        },

        getUserDetail: (userDetail = {}) => {
            let action = {
                type: TYPE.GET_USER_DETAIL,
                payload: userDetail,
            }
            dispatch(action);
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)
