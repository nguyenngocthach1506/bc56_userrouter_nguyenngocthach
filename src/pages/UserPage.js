import React, { Component } from 'react'
import Form from "../components/Form/Form";
import User from "../components/User/User";
import Loading from '../components/Loading/Loading';

export default class UserPage extends Component {
    state = {
        isLoading: false,
    }

    setLoading = (value = false) => {
        this.setState({ isLoading: value })
    }

    render() {
        return (
            (<div className='container-lg rounded my-lg-3'>
                {(this.state.isLoading) ? <Loading></Loading> : <></>}
                <div className='my-4 p-2 bg-light rounded border border-2' >
                    <Form setLoading={this.setLoading}></Form>
                </div>
                <div className='my-2 p-2 bg-light rounded border border-2' >
                    <User setLoading={this.setLoading}></User>
                </div>
            </div >)
        )
    }
}
