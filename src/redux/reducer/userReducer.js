import TYPE from "../constaint/type";

const initialState = {
    users: [],
    userDetail: { id: "", name: "", account: "", password: "" },
}

export const userReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case TYPE.GET_LIST_USER:
            {
                state.users = payload;
                return { ...state, users: payload };
            }
        case TYPE.GET_USER_DETAIL:
            {
                return { ...state, userDetail: payload };
            }
        default:
            return state;
    }
}
