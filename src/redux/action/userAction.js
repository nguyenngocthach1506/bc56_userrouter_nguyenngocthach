import TYPE from "../constaint/type";
import userAPI from "../../service/Api/userApi";

export const getUserAction = () => { // thunk action
    return (dispatch, getState) => {
        userAPI.getAllUsers().then(users => {
            let action = {
                type: TYPE.GET_LIST_USER,
                payload: users,
            }
            dispatch(action);
        });
    }
}
