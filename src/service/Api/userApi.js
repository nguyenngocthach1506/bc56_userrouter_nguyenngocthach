import axiosClient from "./axiosClient";


/* baseURL: https://64c62b5ec853c26efadb28e8.mockapi.io */
/* GET> url: null || /id */
/* POST: url:/users */
/* PUT: url:/users/id */
/* DELETE: url:/users/id */

class userApi {
    url = "/users/"
    getAllUsers = async () => {
        return await axiosClient.get(this.url).then(res => res.data).catch(console.error);
    }

    removeUser = async (id) => {
        return await axiosClient.delete(this.url + id).then(res => res).catch(console.error);
    }

    postUser = async (user = {}) => {
        return await axiosClient.post(this.url, user).then(res => res).catch(console.error);
    }

    putUser = async (id, user = {}) => {
        return await axiosClient.put(this.url + id, user).then(res => res).catch(console.error);
    }
}


const userAPI = new userApi();
export default userAPI;