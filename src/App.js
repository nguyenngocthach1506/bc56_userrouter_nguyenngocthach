import "../node_modules/bootstrap/dist/css/bootstrap.css";
/* */
import Header from "./components/Header/Header";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import UserPage from "./pages/UserPage";

function App() {
  return (
    <BrowserRouter>
      <Header></Header>
      <Routes>
        <Route path="/" element={<HomePage />}></Route>
        <Route path="/User" element={<UserPage />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
